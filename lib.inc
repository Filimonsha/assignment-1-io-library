section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
    xor rdi, rdi 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
    .counter:
        cmp byte[rdi + rax], 0
        je .end
        inc rax
        jmp .counter
        
    .end:
	    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    push rsi
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall
    pop rsi
    pop rdi
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push di
    mov rsi, rsp
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    syscall

    pop di
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, 10
    mov r9, rsp
    push byte 0
.loop:
    xor rdx, rdx
    div rsi
    add dl, "0"
    dec rsp
    mov byte[rsp], dl
    test rax, rax
    jnz .loop
    mov rdi, rsp
    call print_string
    mov rsp, r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    cmp rax, 0
    jns .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.print:
    jmp print_uint
         

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
.loop:
    mov dl, byte[rdi + rcx]
    cmp dl, byte[rsi + rcx]
    jne .end
    inc rcx
    cmp dl, 0
    jne .loop
    inc rax
.end:
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    syscall

    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    mov r8, rdi         ; starting adress
    mov r9, rsi         ; length
    .loop:
        push rcx
        call read_char
        pop rcx
        cmp rax, 0x20
        je .spaces_check
        cmp rax, 0x9
        je .spaces_check
        cmp rax, 0xA
        je .spaces_check
        cmp rax, 0
        jz .end
        cmp r9, rcx
        je .overflow
        mov byte[r8 + rcx], al
        inc rcx
        jmp .loop
    .spaces_check:
        cmp rcx, 0
        je .loop
    .end:
        mov byte[r8 + rcx], 0
        mov rax, r8
        mov rdx, rcx
        ret 
    .overflow:
        mov rax, 0
        xor rdx, rdx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    xor rcx, rcx
    xor rsi, rsi
    mov r8, 10
    .loop:
        mov cl, byte[rdi + rsi]
        xor cl, "0"
        cmp cl, 9
        ja .break
        mul r8
        add rax, rcx
        inc rsi
        jmp .loop
    .break:
        mov rdx, rsi
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    mov cl, byte[rdi]
    cmp cl, "-"
    jne parse_uint          
    inc rdi     
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jae .break
    xor rax, rax
    .loop:
        mov cl, byte[rdi + rax]
        mov byte[rsi + rax], cl
        inc rax
        cmp cl, 0
        jne .loop
        ret
    .break:
        xor rax, rax
        ret
